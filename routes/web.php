<?php

use Illuminate\Support\Facades\Route;
// use Illuminate\Support\Facades\DB;
use \App\Models\Post;
use \App\Models\User;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::get('/posts', '\App\Http\Controllers\PostsController@index');

// Route::get('/post/{id}', '\App\Http\Controllers\PostsController@show');

// Route::resource('posts', '\App\Http\Controllers\PostsController');

// Route::get('/contacts', '\App\Http\Controllers\PostsController@contacts');
// Route::get('/post/{id}/{username}/{pass}', '\App\Http\Controllers\PostsController@show_post');

// Raw DBp
// Route::get('/insert', function () {
//     DB::insert('insert into posts(title, content) values(?, ?)', ['PHP with Laravel', 'Laravel is the best']);
// });
// Route::get('/read', function () {
//     $result = DB::select('select * from posts where id = ?', [1]);
//     return var_dump($result);
//     die();
//     foreach($result as $row) {
//         return $row->title;
//     }
// });
// Route::get('/update', function () {
//     $updated = DB::update('update posts set title = "Updated title" where id = ?', [1]);
//     return $updated;
// });
// Route::get('/delete', function () {
//     $deleted = DB::delete('delete from posts where id = ?', [2]);
//     return $deleted;
// });

/*
|-----------------------------------------------------------
|  ELOQUENT
|-----------------------------------------------------------
*/

Route::get('/read', function () {
    $posts = Post::all();
    foreach($posts as $post) {
        return $post->title;
    }
});

Route::get('/find', function () {
    $posts = Post::find(1);
    return $posts->title;
});

Route::get('/findwhere', function () {
    $posts = Post::where('id', 1)->orderBy('id', 'desc')->take(1)->get();
    return $posts;
});

Route::get('/findmore', function () {
    $posts = Post::findOrFail(1);
    // $posts = Post::where('user_count', '<', 50)->firstOrFail();
    return $posts;
});

Route::get('/basicinsert', function () {
    $post = new Post;
    $post->title = 'New Eloquent Title';
    $post->content = 'Eloquent content';
    $post->save();
});

Route::get('/basicupdate', function () {
    $post = Post::find(1);
    $post->title = 'Updated Eloquent Title';
    $post->content = 'Updated Eloquent content';
    $post->save();
});

Route::get('/basiccreate', function () {
    Post::create(['title'=>'the Create Method', 'content'=>'Wow I\'am learning alot today']);
});

Route::get('/update2', function() {
    Post::where('id', 1)->where('is_admin', 0)->update(['title'=>'New Title 2', 'content'=>'New Content 2']);
});

Route::get('/delete2', function() {
    $post = Post::find(1);
    $post->delete();
});

Route::get('/delete3', function() {
    Post::destroy([4, 5]);
});

Route::get('softdelete', function() {
    Post::find(5)->delete();
});

Route::get('readsoftdelete', function() {
    // $post = Post::find(5);
    // $post = Post::withTrashed()->where('id', 5)->get();
    $post = Post::onlyTrashed()->get();
    return $post;
});

Route::get('/restore', function() {
    Post::withTrashed()->where('id', 5)->restore();
});

Route::get('/forcedelete', function() {
    Post::withTrashed()->where('id', 5)->forceDelete();
});

/*
|-----------------------------------------------------------
|  ELOQUENT RELATIONSHIPS
|-----------------------------------------------------------
*/

// hasOne relationship

Route::get('/user/{id}/post', function ($id) {
    return User::find($id)->post;
});

// belongsTo relationship

Route::get('/post/{id}/user', function($id) {
    return Post::find($id)->user;
});

// hasMany relationship

Route::get('/posts', function() {
    $user = User::find(1);

    foreach($user->posts as $post) {
        echo $post->title . "<br>";
    }
    // return Post::find($user)->post; 
});

// belongsToMany relationship

Route::get('/user/{id}/role', function($id) {
    $user = User::find($id);
    
    foreach($user->roles as $role) {
        echo $role->name . "<br>";
    }
});

Route::get('/user/pivot', function() {
    $user = User::find(1);

    foreach($user->roles as $role) {
        echo $role->pivot;
    }
});